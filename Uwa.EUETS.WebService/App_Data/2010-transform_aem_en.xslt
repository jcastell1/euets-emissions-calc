﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
	<!-- transform aem v3.8.019 -->
	<xsl:output method="xml" indent="no" omit-xml-declaration="no"/>

	<xsl:template match="/">
		<workbook>
			<worksheet name="Identification and description">
				<cell id="H6">
					<xsl:attribute name="value">
						<xsl:value-of select="report/year"/>
					</xsl:attribute>
				</cell>

				<xsl:apply-templates select="report/operator" mode="identification" />
			</worksheet>

			<worksheet name="Emissions overview">
				<xsl:apply-templates select="report/operator" mode="emissionsoverview" />
			</worksheet>

			<worksheet name="Emissions Data">
				<xsl:apply-templates select="report/emissions/domestic_legs" mode="domestic_legs" />
				<xsl:apply-templates select="report/emissions/departing_legs" mode="departing_legs" />
				<xsl:apply-templates select="report/emissions/arriving_legs" mode="arriving_legs" />
			</worksheet>

			<worksheet name="Aircraft Data">
				<xsl:apply-templates select="report/registries" mode="registries" />
			</worksheet>

			<worksheet name="Annex">
				<xsl:apply-templates select="report/emissions_per_aerodrome_pair" mode="annex" />
			</worksheet>
		</workbook>
	</xsl:template>

	<xsl:template match="/report/operator" mode="identification">
		<cell id="H11">
			<xsl:attribute name="value">
				<xsl:value-of select="name"/>
			</xsl:attribute>
		</cell>

		<cell id="H14">
			<xsl:attribute name="value">
				<xsl:value-of select="id"/>
			</xsl:attribute>
		</cell>

		<cell id="H17">
			<xsl:attribute name="value">
				<xsl:value-of select="name_commission"/>
			</xsl:attribute>
		</cell>

		<cell id="H20" default="n/a" >
			<xsl:attribute name="value">
				<xsl:value-of select="icao_designator"/>
			</xsl:attribute>
		</cell>

		<cell id="H23" default="n/a" >
			<xsl:attribute name="value">
				<xsl:value-of select="aircraft_registrations"/>
			</xsl:attribute>
		</cell>

		<cell id="H26">
			<xsl:attribute name="value">
				<xsl:value-of select="euets_mbr_state"/>
			</xsl:attribute>
		</cell>

		<cell id="H28" default="n/a">
			<xsl:attribute name="value">
				<xsl:value-of select="euets_mbr_state_auth"/>
			</xsl:attribute>
		</cell>

		<cell id="H31">
			<xsl:attribute name="value">
				<xsl:value-of select="eu_aoc"/>
			</xsl:attribute>
		</cell>

		<cell id="H32">
			<xsl:attribute name="value">
				<xsl:value-of select="eu_aoc_auth"/>
			</xsl:attribute>
		</cell>

		<cell id="H33">
			<xsl:attribute name="value">
				<xsl:value-of select="eu_op_license"/>
			</xsl:attribute>
		</cell>

		<cell id="H34">
			<xsl:attribute name="value">
				<xsl:value-of select="eu_op_license_auth"/>
			</xsl:attribute>
		</cell>

		<cell id="H37">
			<xsl:attribute name="value">
				<xsl:value-of select="address1"/>
			</xsl:attribute>
		</cell>

		<cell id="H38">
			<xsl:attribute name="value">
				<xsl:value-of select="address2"/>
			</xsl:attribute>
		</cell>

		<cell id="H39">
			<xsl:attribute name="value">
				<xsl:value-of select="city"/>
			</xsl:attribute>
		</cell>

		<cell id="H40">
			<xsl:attribute name="value">
				<xsl:value-of select="state"/>
			</xsl:attribute>
		</cell>

		<cell id="H41">
			<xsl:attribute name="value">
				<xsl:value-of select="zip"/>
			</xsl:attribute>
		</cell>

		<cell id="H42">
			<xsl:attribute name="value">
				<xsl:value-of select="country"/>
			</xsl:attribute>
		</cell>

		<cell id="H43">
			<xsl:attribute name="value">
				<xsl:value-of select="phone"/>
			</xsl:attribute>
		</cell>

		<cell id="H44">
			<xsl:attribute name="value">
				<xsl:value-of select="email"/>
			</xsl:attribute>
		</cell>

		<cell id="H48">
			<xsl:attribute name="value">
				<xsl:value-of select="contact_title"/>
			</xsl:attribute>
		</cell>

		<cell id="H49">
			<xsl:attribute name="value">
				<xsl:value-of select="contact_fname"/>
			</xsl:attribute>
		</cell>

		<cell id="H50">
			<xsl:attribute name="value">
				<xsl:value-of select="contact_sname"/>
			</xsl:attribute>
		</cell>

		<cell id="H51">
			<xsl:attribute name="value">
				<xsl:value-of select="contact_job_title"/>
			</xsl:attribute>
		</cell>

		<cell id="H53">
			<xsl:attribute name="value">
				<xsl:value-of select="contact_company"/>
			</xsl:attribute>
		</cell>

		<cell id="H54">
			<xsl:attribute name="value">
				<xsl:value-of select="contact_phone"/>
			</xsl:attribute>
		</cell>

		<cell id="H55">
			<xsl:attribute name="value">
				<xsl:value-of select="contact_email"/>
			</xsl:attribute>
		</cell>

		<cell id="H59">
			<xsl:attribute name="value">
				<xsl:value-of select="contact_email"/>
			</xsl:attribute>
		</cell>

		<cell id="H59">
			<xsl:attribute name="value">
				<xsl:value-of select="receipt_title"/>
			</xsl:attribute>
		</cell>

		<cell id="H60">
			<xsl:attribute name="value">
				<xsl:value-of select="receipt_fname"/>
			</xsl:attribute>
		</cell>

		<cell id="H61">
			<xsl:attribute name="value">
				<xsl:value-of select="receipt_sname"/>
			</xsl:attribute>
		</cell>

		<cell id="H62">
			<xsl:attribute name="value">
				<xsl:value-of select="receipt_email"/>
			</xsl:attribute>
		</cell>

		<cell id="H63">
			<xsl:attribute name="value">
				<xsl:value-of select="receipt_phone"/>
			</xsl:attribute>
		</cell>

		<cell id="H64">
			<xsl:attribute name="value">
				<xsl:value-of select="receipt_address1"/>
			</xsl:attribute>
		</cell>

		<cell id="H65">
			<xsl:attribute name="value">
				<xsl:value-of select="receipt_address2"/>
			</xsl:attribute>
		</cell>

		<cell id="H66">
			<xsl:attribute name="value">
				<xsl:value-of select="receipt_city"/>
			</xsl:attribute>
		</cell>

		<cell id="H67">
			<xsl:attribute name="value">
				<xsl:value-of select="receipt_state"/>
			</xsl:attribute>
		</cell>

		<cell id="H68">
			<xsl:attribute name="value">
				<xsl:value-of select="receipt_zip"/>
			</xsl:attribute>
		</cell>

		<cell id="H69">
			<xsl:attribute name="value">
				<xsl:value-of select="receipt_country"/>
			</xsl:attribute>
		</cell>

		<cell id="H74">
			<xsl:attribute name="value">
				<xsl:value-of select="verifier_company"/>
			</xsl:attribute>
		</cell>

		<cell id="H75">
			<xsl:attribute name="value">
				<xsl:value-of select="verifier_address1"/>
			</xsl:attribute>
		</cell>

		<cell id="H76">
			<xsl:attribute name="value">
				<xsl:value-of select="verifier_address2"/>
			</xsl:attribute>
		</cell>

		<cell id="H77">
			<xsl:attribute name="value">
				<xsl:value-of select="verifier_city"/>
			</xsl:attribute>
		</cell>

		<cell id="H78">
			<xsl:attribute name="value">
				<xsl:value-of select="verifier_state"/>
			</xsl:attribute>
		</cell>

		<cell id="H79">
			<xsl:attribute name="value">
				<xsl:value-of select="verifier_zip"/>
			</xsl:attribute>
		</cell>

		<cell id="H80">
			<xsl:attribute name="value">
				<xsl:value-of select="verifier_country"/>
			</xsl:attribute>
		</cell>

		<cell id="H84">
			<xsl:attribute name="value">
				<xsl:value-of select="verifier_title"/>
			</xsl:attribute>
		</cell>

		<cell id="H85">
			<xsl:attribute name="value">
				<xsl:value-of select="verifier_fname"/>
			</xsl:attribute>
		</cell>

		<cell id="H86">
			<xsl:attribute name="value">
				<xsl:value-of select="verifier_sname"/>
			</xsl:attribute>
		</cell>

		<cell id="H87">
			<xsl:attribute name="value">
				<xsl:value-of select="verifier_email"/>
			</xsl:attribute>
		</cell>

		<cell id="H88">
			<xsl:attribute name="value">
				<xsl:value-of select="verifier_phone"/>
			</xsl:attribute>
		</cell>

		<cell id="H91">
			<xsl:attribute name="value">
				<xsl:value-of select="verifier_acred_mbr_state"/>
			</xsl:attribute>
		</cell>

		<cell id="H92">
			<xsl:attribute name="value">
				<xsl:value-of select="verifier_reg_nbr"/>
			</xsl:attribute>
		</cell>
	</xsl:template>

	<xsl:template match="/report/operator" mode="emissionsoverview">
		<cell id="H6">
			<xsl:attribute name="value">
				<xsl:value-of select="plan_ref"/>
			</xsl:attribute>
		</cell>

		<cell id="H8">
			<xsl:attribute name="value">
				<xsl:value-of select="plan_version"/>
			</xsl:attribute>
		</cell>

		<cell id="L12" type="bool">
			<xsl:attribute name="value">
				<xsl:value-of select="plan_deviation"/>
			</xsl:attribute>
		</cell>

		<cell id="C15">
			<xsl:attribute name="value">
				<xsl:value-of select="deviation_detail"/>
			</xsl:attribute>
		</cell>

		<cell id="J19">
			<xsl:attribute name="value">
				<xsl:value-of select="total_flights"/>
			</xsl:attribute>
		</cell>

		<cell id="I25">
			<xsl:attribute name="value">
				<xsl:value-of select="jet_a_total_consumption"/>
			</xsl:attribute>
		</cell>

		<cell id="I26">
			<xsl:attribute name="value">
				<xsl:value-of select="jet_b_total_consumption"/>
			</xsl:attribute>
		</cell>

		<cell id="I27">
			<xsl:attribute name="value">
				<xsl:value-of select="avgas_total_consumption"/>
			</xsl:attribute>
		</cell>

		<cell id="E42">
			<xsl:attribute name="value">
				<xsl:value-of select="jet_a_ac_types"/>
			</xsl:attribute>
		</cell>

		<cell id="E43">
			<xsl:attribute name="value">
				<xsl:value-of select="jet_b_ac_types"/>
			</xsl:attribute>
		</cell>

		<cell id="E44">
			<xsl:attribute name="value">
				<xsl:value-of select="avgas_ac_types"/>
			</xsl:attribute>
		</cell>

		<cell id="L58" type="bool">
			<xsl:attribute name="value">
				<xsl:value-of select="simple_approach"/>
			</xsl:attribute>
		</cell>

		<cell id="F63">
			<xsl:attribute name="value">
				<xsl:value-of select="jan_apr_flights"/>
			</xsl:attribute>
		</cell>

		<cell id="F64">
			<xsl:attribute name="value">
				<xsl:value-of select="may_aug_flights"/>
			</xsl:attribute>
		</cell>

		<cell id="F65">
			<xsl:attribute name="value">
				<xsl:value-of select="sep_dec_flights"/>
			</xsl:attribute>
		</cell>

		<cell id="L78" type="bool">
			<xsl:attribute name="value">
				<xsl:value-of select="data_gaps"/>
			</xsl:attribute>
		</cell>

		<cell id="I80">
			<xsl:attribute name="value">
				<xsl:value-of select="data_gaps_tonnes_co2"/>
			</xsl:attribute>
		</cell>

		<cell id="L87" type="bool">
			<xsl:attribute name="value">
				<xsl:value-of select="biomass"/>
			</xsl:attribute>
		</cell>
	</xsl:template>

	<xsl:template match="/report/emissions/domestic_legs" mode="domestic_legs">
		<xsl:for-each select="leg">
			<xsl:choose>
				<xsl:when test="member_state='Austria'">
					<cell id="E24">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</xsl:when>
				<xsl:when test="member_state='Belgium'">
					<cell id="E25">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</xsl:when>
				<xsl:when test="member_state='Bulgaria'">
					<cell id="E26">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</xsl:when>
				<xsl:when test="member_state='Cyprus'">
					<cell id="E27">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</xsl:when>
				<xsl:when test="member_state='Czech Republic'">
					<cell id="E28">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</xsl:when>
				<xsl:when test="member_state='Denmark'">
					<cell id="E29">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</xsl:when>
				<xsl:when test="member_state='Estonia'">
					<cell id="E30">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</xsl:when>
				<xsl:when test="member_state='Finland'">
					<cell id="E31">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</xsl:when>
				<xsl:when test="member_state='France'">
					<cell id="E32">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</xsl:when>
				<xsl:when test="member_state='Germany'">
					<cell id="E33">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</xsl:when>
				<xsl:when test="member_state='Greece'">
					<cell id="E34">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</xsl:when>
				<xsl:when test="member_state='Hungary'">
					<cell id="E35">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</xsl:when>
				<xsl:when test="member_state='Iceland'">
					<cell id="E36">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</xsl:when>
				<xsl:when test="member_state='Ireland'">
					<cell id="E37">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</xsl:when>
				<xsl:when test="member_state='Italy'">
					<cell id="E38">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</xsl:when>
				<xsl:when test="member_state='Latvia'">
					<cell id="E39">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</xsl:when>
				<xsl:when test="member_state='Liechtenstein'">
					<cell id="E40">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</xsl:when>
				<xsl:when test="member_state='Lithuania'">
					<cell id="E41">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</xsl:when>
				<xsl:when test="member_state='Luxembourg'">
					<cell id="E42">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</xsl:when>
				<xsl:when test="member_state='Malta'">
					<cell id="E43">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</xsl:when>
				<xsl:when test="member_state='Netherlands'">
					<cell id="E44">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</xsl:when>
				<xsl:when test="member_state='Norway'">
					<cell id="E45">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</xsl:when>
				<xsl:when test="member_state='Poland'">
					<cell id="E46">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</xsl:when>
				<xsl:when test="member_state='Portugal'">
					<cell id="E47">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</xsl:when>
				<xsl:when test="member_state='Romania'">
					<cell id="E48">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</xsl:when>
				<xsl:when test="member_state='Slovakia'">
					<cell id="E49">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</xsl:when>
				<xsl:when test="member_state='Slovenia'">
					<cell id="E50">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</xsl:when>
				<xsl:when test="member_state='Spain'">
					<cell id="E51">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</xsl:when>
				<xsl:when test="member_state='Sweden'">
					<cell id="E52">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</xsl:when>
				<xsl:when test="member_state='United Kingdom'">
					<cell id="E53">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</xsl:when>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="report/emissions/departing_legs" mode="departing_legs">
		<cell type="table" range="UWA_DPLEGS_RANGE">
			<xsl:for-each select="leg">
				<row>
					<cell col="C">
						<xsl:attribute name="value">
							<xsl:value-of select="departure_state"/>
						</xsl:attribute>
					</cell>

					<cell col="D">
						<xsl:attribute name="value">
							<xsl:value-of select="arrival_state"/>
						</xsl:attribute>
					</cell>

					<cell col="E">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</row>
			</xsl:for-each>
		</cell>
	</xsl:template>

	<xsl:template match="report/emissions/arriving_legs" mode="arriving_legs">
		<cell type="table" range="UWA_ARLEGS_RANGE">
			<xsl:for-each select="leg">
				<row>
					<cell col="C">
						<xsl:attribute name="value">
							<xsl:value-of select="departure_state"/>
						</xsl:attribute>
					</cell>

					<cell col="D">
						<xsl:attribute name="value">
							<xsl:value-of select="arrival_state"/>
						</xsl:attribute>
					</cell>

					<cell col="E">
						<xsl:attribute name="value">
							<xsl:value-of select="jet_a_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</row>
			</xsl:for-each>
		</cell>
	</xsl:template>

	<xsl:template match="report/registries" mode="registries">
		<cell type="table" range="UWA_ACDATA_RANGE">
			<xsl:for-each select="registry">
				<row>
					<cell col="C">
						<xsl:attribute name="value">
							<xsl:value-of select="aircraft_type"/>
						</xsl:attribute>
					</cell>

					<cell col="D">
						<xsl:attribute name="value">
							<xsl:value-of select="aircraft_subtype"/>
						</xsl:attribute>
					</cell>

					<cell col="E">
						<xsl:attribute name="value">
							<xsl:value-of select="tail_number"/>
						</xsl:attribute>
					</cell>
				</row>
			</xsl:for-each>
		</cell>
	</xsl:template>

	<xsl:template match="report/emissions_per_aerodrome_pair" mode="annex">
		<cell id="I8" value="Y" />

		<cell type="table" range="UWA_EMAP_RANGE">
			<xsl:for-each select="aerodrome_pair">
				<row>
					<cell col="C">
						<xsl:attribute name="value">
							<xsl:value-of select="aerodrome1"/>
						</xsl:attribute>
					</cell>

					<cell col="D">
						<xsl:attribute name="value">
							<xsl:value-of select="aerodrome2"/>
						</xsl:attribute>
					</cell>

					<cell col="E">
						<xsl:attribute name="value">
							<xsl:value-of select="total_flights"/>
						</xsl:attribute>
					</cell>

					<cell col="F">
						<xsl:attribute name="value">
							<xsl:value-of select="total_tonnes_co2"/>
						</xsl:attribute>
					</cell>
				</row>
			</xsl:for-each>
		</cell>
	</xsl:template>

</xsl:stylesheet>
