﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
	<!-- transform tkm v3.8.017 -->
	<xsl:output method="xml" indent="no" omit-xml-declaration="no"/>

	<xsl:template match="/">
		<workbook>
			<worksheet name="Identification and description">
				<cell id="H6">
					<xsl:attribute name="value">
						<xsl:value-of select="report/year"/>
					</xsl:attribute>
				</cell>

				<xsl:apply-templates select="report/operator" mode="identification" />
			</worksheet>

			<worksheet name="Aircraft Data">
				<xsl:apply-templates select="report/registries" mode="registries" />
			</worksheet>

			<worksheet name="Tonne-kilometre Data">
				<xsl:apply-templates select="report/tonne_km" mode="tonne_km" />
			</worksheet>
		</workbook>
	</xsl:template>

	<xsl:template match="/report/operator" mode="identification">
		<cell id="H11">
			<xsl:attribute name="value">
				<xsl:value-of select="name"/>
			</xsl:attribute>
		</cell>

		<cell id="H14">
			<xsl:attribute name="value">
				<xsl:value-of select="id"/>
			</xsl:attribute>
		</cell>

		<cell id="H17">
			<xsl:attribute name="value">
				<xsl:value-of select="name_commission"/>
			</xsl:attribute>
		</cell>

		<cell id="H20" default="n/a" >
			<xsl:attribute name="value">
				<xsl:value-of select="icao_designator"/>
			</xsl:attribute>
		</cell>

		<cell id="H23" default="n/a" >
			<xsl:attribute name="value">
				<xsl:value-of select="icao_designator"/>
			</xsl:attribute>
		</cell>

		<cell id="H23" default="n/a" >
			<xsl:attribute name="value">
				<xsl:value-of select="aircraft_registrations"/>
			</xsl:attribute>
		</cell>

		<cell id="H26">
			<xsl:attribute name="value">
				<xsl:value-of select="euets_mbr_state"/>
			</xsl:attribute>
		</cell>

		<cell id="H28" default="n/a">
			<xsl:attribute name="value">
				<xsl:value-of select="euets_mbr_state_auth"/>
			</xsl:attribute>
		</cell>

		<cell id="H31">
			<xsl:attribute name="value">
				<xsl:value-of select="eu_aoc"/>
			</xsl:attribute>
		</cell>

		<cell id="H32">
			<xsl:attribute name="value">
				<xsl:value-of select="eu_aoc_auth"/>
			</xsl:attribute>
		</cell>

		<cell id="H33">
			<xsl:attribute name="value">
				<xsl:value-of select="eu_op_license"/>
			</xsl:attribute>
		</cell>

		<cell id="H34">
			<xsl:attribute name="value">
				<xsl:value-of select="eu_op_license_auth"/>
			</xsl:attribute>
		</cell>

		<cell id="H37">
			<xsl:attribute name="value">
				<xsl:value-of select="address1"/>
			</xsl:attribute>
		</cell>

		<cell id="H38">
			<xsl:attribute name="value">
				<xsl:value-of select="address2"/>
			</xsl:attribute>
		</cell>

		<cell id="H39">
			<xsl:attribute name="value">
				<xsl:value-of select="city"/>
			</xsl:attribute>
		</cell>

		<cell id="H40">
			<xsl:attribute name="value">
				<xsl:value-of select="state"/>
			</xsl:attribute>
		</cell>

		<cell id="H41">
			<xsl:attribute name="value">
				<xsl:value-of select="zip"/>
			</xsl:attribute>
		</cell>

		<cell id="H42">
			<xsl:attribute name="value">
				<xsl:value-of select="country"/>
			</xsl:attribute>
		</cell>

		<cell id="H43">
			<xsl:attribute name="value">
				<xsl:value-of select="phone"/>
			</xsl:attribute>
		</cell>

		<cell id="H44">
			<xsl:attribute name="value">
				<xsl:value-of select="email"/>
			</xsl:attribute>
		</cell>

		<cell id="H48">
			<xsl:attribute name="value">
				<xsl:value-of select="contact_title"/>
			</xsl:attribute>
		</cell>

		<cell id="H49">
			<xsl:attribute name="value">
				<xsl:value-of select="contact_fname"/>
			</xsl:attribute>
		</cell>

		<cell id="H50">
			<xsl:attribute name="value">
				<xsl:value-of select="contact_sname"/>
			</xsl:attribute>
		</cell>

		<cell id="H51">
			<xsl:attribute name="value">
				<xsl:value-of select="contact_job_title"/>
			</xsl:attribute>
		</cell>

		<cell id="H53">
			<xsl:attribute name="value">
				<xsl:value-of select="contact_company"/>
			</xsl:attribute>
		</cell>

		<cell id="H54">
			<xsl:attribute name="value">
				<xsl:value-of select="contact_phone"/>
			</xsl:attribute>
		</cell>

		<cell id="H55">
			<xsl:attribute name="value">
				<xsl:value-of select="contact_email"/>
			</xsl:attribute>
		</cell>

		<cell id="H59">
			<xsl:attribute name="value">
				<xsl:value-of select="contact_email"/>
			</xsl:attribute>
		</cell>

		<cell id="H59">
			<xsl:attribute name="value">
				<xsl:value-of select="receipt_title"/>
			</xsl:attribute>
		</cell>

		<cell id="H60">
			<xsl:attribute name="value">
				<xsl:value-of select="receipt_fname"/>
			</xsl:attribute>
		</cell>

		<cell id="H61">
			<xsl:attribute name="value">
				<xsl:value-of select="receipt_sname"/>
			</xsl:attribute>
		</cell>

		<cell id="H62">
			<xsl:attribute name="value">
				<xsl:value-of select="receipt_email"/>
			</xsl:attribute>
		</cell>

		<cell id="H63">
			<xsl:attribute name="value">
				<xsl:value-of select="receipt_phone"/>
			</xsl:attribute>
		</cell>

		<cell id="H64">
			<xsl:attribute name="value">
				<xsl:value-of select="receipt_address1"/>
			</xsl:attribute>
		</cell>

		<cell id="H65">
			<xsl:attribute name="value">
				<xsl:value-of select="receipt_address2"/>
			</xsl:attribute>
		</cell>

		<cell id="H66">
			<xsl:attribute name="value">
				<xsl:value-of select="receipt_city"/>
			</xsl:attribute>
		</cell>

		<cell id="H67">
			<xsl:attribute name="value">
				<xsl:value-of select="receipt_state"/>
			</xsl:attribute>
		</cell>

		<cell id="H68">
			<xsl:attribute name="value">
				<xsl:value-of select="receipt_zip"/>
			</xsl:attribute>
		</cell>

		<cell id="H69">
			<xsl:attribute name="value">
				<xsl:value-of select="receipt_country"/>
			</xsl:attribute>
		</cell>

		<cell id="H74">
			<xsl:attribute name="value">
				<xsl:value-of select="verifier_company"/>
			</xsl:attribute>
		</cell>

		<cell id="H75">
			<xsl:attribute name="value">
				<xsl:value-of select="verifier_address1"/>
			</xsl:attribute>
		</cell>

		<cell id="H76">
			<xsl:attribute name="value">
				<xsl:value-of select="verifier_address2"/>
			</xsl:attribute>
		</cell>

		<cell id="H77">
			<xsl:attribute name="value">
				<xsl:value-of select="verifier_city"/>
			</xsl:attribute>
		</cell>

		<cell id="H78">
			<xsl:attribute name="value">
				<xsl:value-of select="verifier_state"/>
			</xsl:attribute>
		</cell>

		<cell id="H79">
			<xsl:attribute name="value">
				<xsl:value-of select="verifier_zip"/>
			</xsl:attribute>
		</cell>

		<cell id="H80">
			<xsl:attribute name="value">
				<xsl:value-of select="verifier_country"/>
			</xsl:attribute>
		</cell>

		<cell id="H84">
			<xsl:attribute name="value">
				<xsl:value-of select="verifier_title"/>
			</xsl:attribute>
		</cell>

		<cell id="H85">
			<xsl:attribute name="value">
				<xsl:value-of select="verifier_fname"/>
			</xsl:attribute>
		</cell>

		<cell id="H86">
			<xsl:attribute name="value">
				<xsl:value-of select="verifier_sname"/>
			</xsl:attribute>
		</cell>

		<cell id="H87">
			<xsl:attribute name="value">
				<xsl:value-of select="verifier_email"/>
			</xsl:attribute>
		</cell>

		<cell id="H88">
			<xsl:attribute name="value">
				<xsl:value-of select="verifier_phone"/>
			</xsl:attribute>
		</cell>

		<cell id="H91">
			<xsl:attribute name="value">
				<xsl:value-of select="verifier_acred_mbr_state"/>
			</xsl:attribute>
		</cell>

		<cell id="H92">
			<xsl:attribute name="value">
				<xsl:value-of select="verifier_reg_nbr"/>
			</xsl:attribute>
		</cell>

		<!-- Below is TKM Only -->
		<cell id="H98">
			<xsl:attribute name="value">
				<xsl:value-of select="plan_ref"/>
			</xsl:attribute>
		</cell>

		<cell id="H100">
			<xsl:attribute name="value">
				<xsl:value-of select="plan_version"/>
			</xsl:attribute>
		</cell>

		<cell id="L104" type="bool">
			<xsl:attribute name="value">
				<xsl:value-of select="plan_deviation"/>
			</xsl:attribute>
		</cell>

		<cell id="C107">
			<xsl:attribute name="value">
				<xsl:value-of select="deviation_detail"/>
			</xsl:attribute>
		</cell>

	</xsl:template>

	<xsl:template match="report/registries" mode="registries">
		<cell type="table" range="UWA_ACDATA_RANGE">
			<xsl:for-each select="registry">
				<row>
					<cell col="C">
						<xsl:attribute name="value">
							<xsl:value-of select="aircraft_type"/>
						</xsl:attribute>
					</cell>

					<cell col="D">
						<xsl:attribute name="value">
							<xsl:value-of select="aircraft_subtype"/>
						</xsl:attribute>
					</cell>

					<cell col="E">
						<xsl:attribute name="value">
							<xsl:value-of select="tail_number"/>
						</xsl:attribute>
					</cell>
				</row>
			</xsl:for-each>
		</cell>
	</xsl:template>

	<xsl:template match="report/tonne_km" mode="tonne_km">
		<cell type="table" range="UWA_TKMDATA_RANGE">
			<xsl:for-each select="aerodrome_pair">
				<row>
					<cell col="C">
						<xsl:attribute name="value">
							<xsl:value-of select="aerodrome1"/>
						</xsl:attribute>
					</cell>

					<cell col="D">
						<xsl:attribute name="value">
							<xsl:value-of select="aerodrome2"/>
						</xsl:attribute>
					</cell>

					<cell col="E">
						<xsl:attribute name="value">
							<xsl:value-of select="gc_distance"/>
						</xsl:attribute>
					</cell>

					<cell col="F">
						<xsl:attribute name="value">
							<xsl:value-of select="total_flights"/>
						</xsl:attribute>
					</cell>

					<cell col="G">
						<xsl:attribute name="value">
							<xsl:value-of select="total_num_pax"/>
						</xsl:attribute>
					</cell>

					<cell col="H">
						<xsl:attribute name="value">
							<xsl:value-of select="mass_pax_bag"/>
						</xsl:attribute>
					</cell>

					<cell col="I">
						<xsl:attribute name="value">
							<xsl:value-of select="mass_freight_mail"/>
						</xsl:attribute>
					</cell>
				</row>
			</xsl:for-each>
		</cell>
	</xsl:template>

</xsl:stylesheet>
