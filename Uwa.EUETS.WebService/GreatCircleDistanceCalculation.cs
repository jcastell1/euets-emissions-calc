﻿using System;

namespace Uwa.EUETS
{
    // this part of the class contains implementations of Great Circle Distance functions
    // taken from the VBA code in the 2018 CORSIA spreadsheet
    // this file corresponds to module e_GCD_Calculation in the spreadsheet
    public sealed partial class EUETS : IEUETSService
    {
        private const double Epsilon = 0.000000000001;


        public double CalculateDistVincenty( double lat1, double lon1, double lat2, double lon2 )
        {
            double rtnval;
            double low_a;
            double low_b;
            double f;
            double l;
            double U1;
            double U2;
            double sinU1;
            double sinU2;
            double cosU1;
            double cosU2;
            double lambda;
            double lambdaP;
            int iterLimit;
            double sinLambda;
            double cosLambda;
            double sinSigma = 0.0;
            double cosSigma = 0.0;
            double sigma = 0.0;
            double sinAlpha;
            double cosSqAlpha = 0.0;
            double cos2SigmaM = 0.0;
            double C;
            double uSq;
            double upper_A;
            double upper_B;
            double deltaSigma;
            double s;

            double P1;
            double P2;
            double P3;


            low_a = 6378137;
            low_b = 6356752.3142;
            f = 1 / 298.257223563;
  
            l = ToRad( lon2 - lon1 );
            U1 = Math.Atan( (1 - f ) * Math.Tan( ToRad(lat1) ) );
            U2 = Math.Atan( (1 - f ) * Math.Tan( ToRad(lat2) ) );
            sinU1 = Math.Sin( U1 );
            cosU1 = Math.Cos( U1 );
            sinU2 = Math.Sin( U2 );
            cosU2 = Math.Cos( U2 );

            lambda = l;
            lambdaP = 2 * Math.PI;
            iterLimit = 100;

            while( ( Math.Abs( lambda - lambdaP ) > Epsilon ) && ( iterLimit > 0 ) )
            {
                iterLimit = iterLimit - 1;

                sinLambda = Math.Sin( lambda );
                cosLambda = Math.Cos( lambda );
                sinSigma = Math.Sqrt( Math.Pow( cosU2 * sinLambda, 2 ) + Math.Pow( ( cosU1 * sinU2 - sinU1 * cosU2 * cosLambda ), 2 ) );
                if( sinSigma == 0 )
                    return 0;

                cosSigma = sinU1 * sinU2 + cosU1 * cosU2 * cosLambda;
                sigma = Atan2( cosSigma, sinSigma );
                sinAlpha = cosU1 * cosU2 * sinLambda / sinSigma;
                cosSqAlpha = 1 - sinAlpha * sinAlpha;

                if( cosSqAlpha == 0 )
                    cos2SigmaM = 0;
                else
                    cos2SigmaM = cosSigma - 2 * sinU1 * sinU2 / cosSqAlpha;


                C = f / 16 * cosSqAlpha * ( 4 + f * ( 4 - 3 * cosSqAlpha ) );
                lambdaP = lambda;

                P1 = -1 + 2 * Math.Pow( cos2SigmaM, 2 );
                P2 = ( sigma + C * sinSigma * ( cos2SigmaM + C * cosSigma * P1 ) );

                lambda = l + ( 1 - C ) * f * sinAlpha * P2;
            }

            if( iterLimit < 1 )
            {
                // iteration limit has been reached, something didn't work
                return -1;
            }

            uSq = cosSqAlpha * ( Math.Pow(low_a, 2) - Math.Pow(low_b, 2) ) / Math.Pow( low_b, 2 );

            P1 = ( 4096 + uSq * ( -768 + uSq * ( 320 - 175 * uSq ) ) );
            upper_A = 1 + uSq / 16384 * P1;

            upper_B = uSq / 1024 * ( 256 + uSq * ( -128 + uSq * ( 74 - 47 * uSq ) ) );

            P1 = ( -3 + 4 * Math.Pow( sinSigma, 2 ) ) * ( -3 + 4 * Math.Pow( cos2SigmaM, 2 ) );
            P2 = upper_B * sinSigma;
            P3 = ( cos2SigmaM + upper_B / 4 * ( cosSigma * ( -1 + 2 * Math.Pow( cos2SigmaM, 2 ) ) - upper_B / 6 * cos2SigmaM * P1 ) );
            deltaSigma = P2 * P3;
            s = low_b * upper_A * ( sigma - deltaSigma );
            rtnval = Math.Round( s, 3 );
    
            return rtnval;
        }


        // appears to only be used in Compute_CO2_Emissions for custom airports - 29 aug jcastell
        /*Function SignIt(Degree_Dec As String ) As Double

            Dim decimalValue As Double
            Dim tempString As String
            tempString = UCase( Trim(Degree_Dec))
            decimalValue = Convert_Decimal( tempString)
            If Right( tempString, 1) = "S" Or Right( tempString, 1) = "W" Then
                decimalValue = decimalValue * -1
            End If
            SignIt = decimalValue
        End Function */


        // doesn't appear to be referenced by anything
        /*Function Convert_Degree( Decimal_Deg) As Variant

            Dim degrees As Variant
            Dim minutes As Variant
            Dim seconds As Variant
            With Application
                degrees = Int( Decimal_Deg)
                minutes = (Decimal_Deg - degrees) * 60
                seconds = Format( ((minutes - Int(minutes)) * 60), "0")
                Convert_Degree = " " & degrees & "° " & Int( minutes) & "' " & seconds + Chr(34)
            End With

        End Function */

        // used only SignIt(), which is used only by custom airports
        // not fully converted to C#
        /*private double Convert_Decimal( string Degree_Deg )
        {
            double decimalDegrees;
            double degrees;
            double minutes;
            double seconds;

            Degree_Deg = Replace( Degree_Deg, "~", "°")

            degrees = Val( Left(Degree_Deg, InStr(1, Degree_Deg, "°") - 1))
  
            minutes = Val( Mid(Degree_Deg, InStr(1, Degree_Deg, "°") + 2, 
                     InStr(1, Degree_Deg, "'") - InStr(1, Degree_Deg, "°") - 2)) / 60
 
            seconds = Val( Mid(Degree_Deg, InStr(1, Degree_Deg, "'") + 2,
                           Len( Degree_Deg) - InStr(1, Degree_Deg, "'") - 2)) / 3600

            decimalDegrees = degrees + minutes + seconds;
            return decimalDegrees;
        }*/


        /// <summary>
        /// convert degrees to radians
        /// </summary>
        /// <param name="degrees">degrees</param>
        /// <returns>radians</returns>
        private double ToRad( double degrees )
        {
            return degrees * ( Math.PI / 180 );
        }


        /// <summary>
        /// two-argument arctangent
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        private double Atan2( double x, double y )
        {
            double rtnval = 0;

            if( y > 0 )
            {
                if( x >= y )
                    rtnval = Math.Atan( y / x );
                else if( x <= -y )
                    rtnval = Math.Atan( y / x ) + Math.PI;
                else
                    rtnval = Math.PI / 2 - Math.Atan( x / y );
            }
            else
            {
                if( x >= -y )
                    rtnval = Math.Atan( y / x );
                else if( x <= y )
                    rtnval = Math.Atan( y / x ) - Math.PI;
                else
                    rtnval = -Math.Atan( x / y ) - Math.PI / 2;
            }
            return rtnval;
        }

    }
}