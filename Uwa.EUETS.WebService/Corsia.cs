﻿using System.Collections.Generic;

namespace Uwa.EUETS
{
    public sealed partial class EUETS : IEUETSService
    {
        // data structures to hold Corsia spreadsheet information
        public class Corsia
        {
            // airport info
            public List<string> Icao  = new List<string>();
            public List<double> Lat   = new List<double>();
            public List<double> Lon   = new List<double>();
            public List<string> State = new List<string>();

            // aircraft type info
            public List<string> AcType      = new List<string>();
            public List<double> Intercept1  = new List<double>();
            public List<double> Slope1      = new List<double>();
            public List<double> Breakpoint1 = new List<double>();
            public List<double> Intercept2  = new List<double>();
            public List<double> Slope2      = new List<double>();
            public List<double> Breakpoint2 = new List<double>();
            public List<double> Intercept3  = new List<double>();
            public List<double> Slope3      = new List<double>();
            public List<byte>   Segments    = new List<byte>();

            public double FueltoCo2 = 3.16;

        }
    }
}