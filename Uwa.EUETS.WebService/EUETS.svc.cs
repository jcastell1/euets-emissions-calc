﻿//------------------------------------------------------------------------------
// Enterprise Applications Group
// Universal Weather and Aviation, Inc.
//
// © 2011 Universal Weather and Aviation, Inc. All Rights Reserved.
//------------------------------------------------------------------------------
// This file contains proprietary information and cannot be used or distributed
// without explicit consent from Universal Weather and Aviation, Inc.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace Uwa.EUETS
{
	/// <summary>
	/// Implementation of EU ETS web service.
	/// </summary>
	[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Single)]
	public sealed partial class EUETS : IEUETSService
	{
		#region Internal fields

		// Constant strings
		private const string _appDataFolder = "App_Data";
		private const string _latestPrefix = "Latest";
		private const string _calculatorFilenameTemplate = "{0}-EmissionCalculator*.xls*";
        private const string _reportFilenameTemplate = "{0}-Report {1}_efta_en.xls";
		private const string _transformationFilenameTemplate = @"App_Data\{0}-transform_{1}_en.xslt";

		// HTTP Response strings
		private const string _successMessage = "OK";
		private const string _errorCode = "-1";
		private const string _emissionResultFormat = "{0}|{1}";

		private const string _textContenType = "text/plain";
		private const string _excelContentType = "application/vnd.ms-excel";
		private const string _responseHeaderContentDisposition = "Content-Disposition";
		private const string _contentDispositionTemplate = "attachment ; filename={0}.xls;";
		private const string _reportFilenamePrefix = "UWA_EUETS_Report_";
		private const string _reportFilenameUnique = "yyyyMMdd_HHmmss";

		// Error messages
		private const string _reportNotCreatedMessageTemplate = "Report could not be created.\n{0}";
		private const string _missingParameters = "Missing parameters.";
        private const string _invalidDistance = "Invalid distance.";
		private const string _calculatorNotFoundTemplate = "Calculator not found for year filter '{0}'.";
		private const string _reportNotFoundTemplate = "Report not found for year filter '{0}'.";

		// XML Elements
		private const string _nameAttributeName = "name";
		private const string _idAttributeName = "id";
		private const string _valueAttributeName = "value";
		private const string _defaultValueAttributeName = "default";
		private const string _typeAttributeName = "type";
		private const string _rangeAttributeName = "range";
		private const string _colAttributeName = "col";

		private const string _typeBoolAttributeValue = "bool";
		private const string _typeBoolYesValue = "Y";
		private const string _typeBoolYesConversionValue = "1";
		private const string _typeBoolNoConversionValue = "2";
		private const string _typeTableAttributeValue = "table";

        private SpreadsheetGear.IWorkbook workbook = null;

        #region Corsia variables

        Dictionary<short, string> CorsiaWorkbooks = null;
        Dictionary<int, Corsia> CorsiaData;
        private const int kgtoton = 1000;

        #endregion


        #endregion


        #region Constructors

        public EUETS()
        {
            CorsiaWorkbooks = new Dictionary<short, string>();
            CorsiaData = new Dictionary<int, Corsia>();

            #region Load Corsia data

            LoadCorsia2018();
            LoadCorsia2019();

            #endregion
        }

        #endregion

        #region IEUETSService Members

        /// <summary>
        /// Gets the calculated emission for the given parameters.
        /// </summary>
        /// <param name="aircraftType">String representing an Aircraft type.</param>
        /// <param name="distance">String representing Distance travelled. String must contain an integer value.</param>
        /// <param name="year">String representing the year of the emissions</param>
        /// <returns>
        /// Returns a stream in the format "value|value". The pipe character '|' is the delimiter.
        /// <para>
        /// If successful, returns values represent estimated fuel consumption and CO2 emissions.
        /// Both values are in Kilograms (Kg).
        /// </para>
        /// <para>
        /// On failure, the returned string will have the format '-1|CAUSE OF ERROR'.
        /// The error is marked by the first value containing a -1. The text after the pipe character
        /// contains more information about the error that occurred.
        /// </para>
        /// </returns>
        public Stream GetEmission(string aircraftType, string distance, string year)
        {
            string responseValue1 = _errorCode;	// Default to error code. This will be overwritten if calculation is successful.
            string responseValue2 = string.Empty;
            string calculationMessage = null;	// Value read from spreadsheet cell representing success of calculation.
            uint distanceInt = 0;

            if (string.IsNullOrWhiteSpace(aircraftType) || string.IsNullOrWhiteSpace(distance))
            {
                responseValue2 = _missingParameters;
            }
            else if( !uint.TryParse( distance, out distanceInt ) || distanceInt > 99999 )
            {
                responseValue2 = _invalidDistance;
            }
            else
            {
                // TODO: Consider caching workbook in memory. (FP20110215)
                try
                {
                    int currentYear = DateTime.Now.Year;
                    int yearValue = int.TryParse(year, out yearValue) ? yearValue : currentYear;
                    string yearPrefix = (yearValue < currentYear) ? yearValue.ToString() : _latestPrefix;

                    string searchPattern = string.Format(_calculatorFilenameTemplate, yearPrefix);
                    string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, _appDataFolder);
                    path = Directory.GetFiles(path, searchPattern).FirstOrDefault();

                    if (!string.IsNullOrWhiteSpace(path))
                    {
                        // TODO: Replace cell access with high-performance API calls. (FP20110215)
                        workbook = SpreadsheetGear.Factory.GetWorkbook(path);
                        SpreadsheetGear.IRange cells = workbook.Worksheets[2].Cells;
                        //SpreadsheetGear.IRange range = workbook.Worksheets[2].Range;

                        // 2017 has different cells than previous years
                        if( workbook.Name.IndexOf( "2017" ) >= 0 )
                        {
                            cells[2, 1].Value = "Nautical mile";
                            cells[15, 0].Value = aircraftType;
                            cells[15, 1].Value = distance;
                            cells[15, 2].Value = 1;
                            cells[15, 3].Value = "N";

                            // see if we get fuel and CO2 amounts
                            responseValue1 = ( cells[15, 4].Value.ToString() == "NA" ) ? "-1" : cells[15, 4].Value.ToString(); // Estimated fuel
                            responseValue2 = ( cells[15, 5].Value.ToString() == "NA" ) ? "Invalid input." : cells[15, 5].Value.ToString();	// Estimated CO2
                        }
                        else // 2016 and previous years
                        {
                            cells[15, 0].Value = aircraftType;	// Copy Aircraft type parameter into spreadsheet.
                            cells[15, 1].Value = distance;

                            calculationMessage = cells[15, 4].Value.ToString().Trim().ToUpper();

                            if( string.CompareOrdinal( calculationMessage, _successMessage ) == 0 )	// Determine if calculation succeeded.
                            {
                                responseValue1 = cells[15, 2].Value.ToString();	// Estimated fuel.
                                responseValue2 = cells[15, 3].Value.ToString();	// Estimated CO2.
                            }
                            else
                            {
                                responseValue2 = calculationMessage;	// The calculation failed, so set error codes.
                            }
                        }
                    }
                    else
                    {
                        responseValue2 = string.Format(_calculatorNotFoundTemplate, yearPrefix);
                    }
                }
                catch (Exception ex)
                {
                    responseValue2 = ex.Message;	// An exception occurred, so set error message.
                }
                finally
                {
                    if (null != workbook)	// Close workbook if opened.
                    {
                        workbook.Close();
                    }
                }
            }

            WebOperationContext.Current.OutgoingResponse.ContentType = _textContenType;
            string returnMessage = string.Format(_emissionResultFormat, responseValue1, responseValue2);
            return new MemoryStream(Encoding.UTF8.GetBytes(returnMessage));
        }


        /// <summary>
        /// Gets the calculated emission from the CORSIA spreadsheet for the given parameters
        /// This is a rewriting of the logic contained the the CORSIA spreadsheet macro,
        /// module b_CO2_Estimation, sub Compute_CO2_Emisions
        /// </summary>
        /// <param name="aircraftType">String representing an Aircraft type.</param>
        /// <param name="origin">The origin ICAO</param>
        /// <param name="destination">The destination ICAO</param>
        /// <param name="year">Year of the emissions</param>
        /// <returns>
        /// Returns a stream in the format "value|value|value". The pipe character '|' is the delimiter.
        /// <para>
        /// If successful, returns values representing the great circle distance (km), CO2 emissions (tonnes),
        /// and whether the flight is subject to the scope of CORSIA
        /// </para>
        /// <para>
        /// On failure, the returned string will have the format '-1|CAUSE OF ERROR'.
        /// The error is marked by the first value containing a -1. The text after the pipe character
        /// contains more information about the error that occurred.
        /// </para>
        /// </returns>
        public Stream GetCorsiaEmission( string aircraftType, string origin, string destination, string year )
        {
            aircraftType = aircraftType.ToUpper();
            origin = origin.ToUpper();
            destination = destination.ToUpper();
            string responseValue1 = _errorCode;	// Default to error code. This will be overwritten if calculation is successful.
            string responseValue2 = "";
            string responseValue3 = "";

            if( string.IsNullOrWhiteSpace( aircraftType ) || string.IsNullOrWhiteSpace( origin ) ||
                string.IsNullOrWhiteSpace( destination ) || string.IsNullOrWhiteSpace( year ) )
            {
                responseValue2 = _missingParameters;
            }
            else
            {
                int yearVal = int.TryParse(year, out yearVal) ? yearVal : 0;

                // use the most recent spreadsheet that is less than or equal to the passed in year
                List<int> corsiaYears = CorsiaData.Keys.ToList();
                int effectiveYear = ( from y in corsiaYears
                                      orderby y descending
                                      where y <= yearVal
                                      select y ).DefaultIfEmpty().Max();

                if( effectiveYear == 0 )
                {
                    responseValue2 = $"Year {year} invalid";
                }
                else
                {
                    double lat1 = 0.0, lon1 = 0.0, lat2 = 0.0, lon2 = 0.0;
                    string originState = "NA";
                    string destinationState = "NA";
                    double gcd = 0.0;
                    double emissions = 0.0;


                    int acIndex = CorsiaData[effectiveYear].AcType.FindIndex( s => s.Equals( aircraftType ) );
                    if( acIndex < 0 )
                    {
                        responseValue2 = $"Aircraft type {aircraftType} not found";
                    }

                    int origIndex = CorsiaData[effectiveYear].Icao.FindIndex( s => s.Equals( origin ) );
                    int destIndex = CorsiaData[effectiveYear].Icao.FindIndex( s => s.Equals( destination ) );
                    if( origIndex < 0 )
                    {
                        if( responseValue2.Length == 0 )
                            responseValue2 = $"Origin ICAO {origin} not found";
                        else
                            responseValue2 += $"; origin ICAO {origin} not found";
                    }
                    if( destIndex < 0 )
                    {
                        if( responseValue2.Length == 0 )
                            responseValue2 = $"Destination ICAO {destination} not found";
                        else
                            responseValue2 += $"; destination ICAO {destination} not found";
                    }

                    if( responseValue2.Length == 0 )
                    {
                        lat1 = CorsiaData[effectiveYear].Lat[origIndex];
                        lon1 = CorsiaData[effectiveYear].Lon[origIndex];
                        originState = CorsiaData[effectiveYear].State[origIndex];
                        lat2 = CorsiaData[effectiveYear].Lat[destIndex];
                        lon2 = CorsiaData[effectiveYear].Lon[destIndex];
                        destinationState = CorsiaData[effectiveYear].State[destIndex];

                        gcd = CalculateDistVincenty( lat1, lon1, lat2, lon2 ) / 1000;
                        double slope = 0.0;
                        double intercept = 0.0;

                        byte segments = CorsiaData[effectiveYear].Segments[acIndex];
                        if( segments == 1 )
                        {
                            slope = CorsiaData[effectiveYear].Slope1[acIndex];
                            intercept = CorsiaData[effectiveYear].Intercept1[acIndex];
                        }
                        else
                        {
                            double breakpoint1 = CorsiaData[effectiveYear].Breakpoint1[acIndex];
                            if( segments == 2 )
                            {
                                if( gcd <= breakpoint1 )
                                {
                                    slope = CorsiaData[effectiveYear].Slope1[acIndex];
                                    intercept = CorsiaData[effectiveYear].Intercept1[acIndex];
                                }
                                else
                                {
                                    slope = CorsiaData[effectiveYear].Slope2[acIndex];
                                    intercept = CorsiaData[effectiveYear].Intercept2[acIndex];
                                }
                            }
                            else
                            {
                                if( gcd <= breakpoint1 )
                                {
                                    slope = CorsiaData[effectiveYear].Slope1[acIndex];
                                    intercept = CorsiaData[effectiveYear].Intercept1[acIndex];
                                }
                                else
                                {
                                    double breakpoint2 = CorsiaData[effectiveYear].Breakpoint2[acIndex];
                                    if( breakpoint1 < gcd && gcd <= breakpoint2 )
                                    {
                                        slope = CorsiaData[effectiveYear].Slope2[acIndex];
                                        intercept = CorsiaData[effectiveYear].Intercept2[acIndex];
                                    }
                                    else
                                    {
                                        slope = CorsiaData[effectiveYear].Slope3[acIndex];
                                        intercept = CorsiaData[effectiveYear].Intercept3[acIndex];
                                    }
                                }
                            }
                        }

                        responseValue1 = Math.Round( gcd, 0 ).ToString();
                        emissions = ( ( ( gcd * slope + intercept ) * CorsiaData[effectiveYear].FueltoCo2 ) / kgtoton );
                        responseValue2 = emissions.ToString();
                        responseValue3 = originState == destinationState ? "No (Domestic)" : "Yes";

                    }
                }

            }

            WebOperationContext.Current.OutgoingResponse.ContentType = _textContenType;
            string returnMessage = string.Format( "{0}|{1}|{2}", responseValue1, responseValue2, responseValue3 );
            return new MemoryStream( Encoding.UTF8.GetBytes( returnMessage ) );
        }


        /// <summary>
        /// Returns an EU ETS emissions report as an Excel file.
        /// <para>
        /// Incoming report data is sent via the HTTP request message as XML embedded as the HTTP message body.
        /// </para>
        /// </summary>
        /// <param name="t">Name of report template to use. Current values can be "aem" and "tkm".</param>
        /// <remarks>
        /// Template names are used as part of the file names used to determine which Excel file and which XSLT file
        /// to use. If these files are not located in the App_Data folder, the call will fail.
        /// </remarks>
        public Stream GetReport(string reportTemplate, string year)
		{
			Stream responseStream = null;
			SpreadsheetGear.IWorkbook workbook = null;
			OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;

			try
			{
				#region Parse XML

				string requestBody = OperationContext.Current.RequestContext.RequestMessage.ToString();

#if DEBUG
				//// Used for debugging.
				//// Normally section is commented out; however, keep inside compiler directive to avoid problems with Release builds.

				//// Dump incoming XML body.
				//string logName = @"D:\Temp\Test Results\Dump " + DateTime.Now.ToString(_reportFilenameUnique) + ".xml";
				//StreamWriter log = new StreamWriter(logName);
				//log.Write(requestBody);
				//log.Close();
#endif

				int currentYear = DateTime.Now.Year;
				int yearValue = int.TryParse(year, out yearValue) ? yearValue : currentYear;
				string yearPrefix = (yearValue < currentYear) ? yearValue.ToString() : _latestPrefix;

				XElement mappedXmlData = TransformXML(requestBody, yearPrefix, reportTemplate);

				#endregion

				#region Spreadsheet manipulation

				string searchPattern = string.Format(_reportFilenameTemplate, yearPrefix, reportTemplate);
				string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, _appDataFolder);

				path = Directory.GetFiles(path, searchPattern).FirstOrDefault();

				if (!string.IsNullOrWhiteSpace(path))
				{
					workbook = SpreadsheetGear.Factory.GetWorkbook(path);	// Create a new Excel workbook.
					if (!FillReportData(mappedXmlData, workbook))	// Call function to fill-out data inside Excel workbook.
					{
						// On error just throw exception to let outer try-catch do its job.
						// No need to add extra error handling code here. We don't need a custom exception message here either.
						throw new ApplicationException();
					}

					// Copy spreadsheet to our memory stream to be returned as part of the response.
					responseStream = workbook.SaveToStream(SpreadsheetGear.FileFormat.Excel8);
					// If we don't rewind here, WCF will read the stream starting at the end returning an empty file.
					responseStream.Seek(0, SeekOrigin.Begin);

				#endregion

					#region HTTP Response preparation

					// Compose 'almost' unique filename. Reports will have to be requested faster than 1 second for names to repeat.
					string reportFilename = _reportFilenamePrefix + DateTime.Now.ToString(_reportFilenameUnique);

					// In order for client agent to recognize that a file is being sent back in the response, we must set the appropriate
					// information in the HTTP header. Namely we must set: Content type, length, date, and filename. The filename
					// is set by using the 'Content-Disposition' header entry.
					// Note that WCF does not have properties for all possible HTTP header types. In such cases, use Headers.Add call.
					response.ContentType = _excelContentType;
					response.ContentLength = responseStream.Length;
					response.LastModified = DateTime.Now;
					response.Headers.Add(_responseHeaderContentDisposition, string.Format(_contentDispositionTemplate, reportFilename));

					#endregion
				}
				else
				{
					// If we get here, we did not find a matching report for year requested.
					response.SetStatusAsNotFound();
					response.ContentType = _textContenType;
					responseStream = new MemoryStream(Encoding.UTF8.GetBytes(string.Format(_reportNotFoundTemplate, yearPrefix)));
				}
			}
			catch (Exception ex)
			{
				// We don't use WebFaultException because it wraps body in XML SOAP elements. We need raw HTTP to be compatible with PERL libraries.
				response.SetStatusAsNotFound();
				response.ContentType = _textContenType;
				responseStream = new MemoryStream(Encoding.UTF8.GetBytes(string.Format(_reportNotCreatedMessageTemplate, ex.Message)));
			}
			finally
			{
				if (null != workbook)	// Close workbook if opened.
				{
					workbook.Close();
				}
			}

			return responseStream;
		}

		/// <summary>
		/// Release allocated resources when called by Garbage collector.
		/// </summary>
		public void Dispose()
		{
			// TODO: Use if we eventually keep workbooks cached in memory. (FP20110215)	
		}

		#endregion

		#region Internal helpers

		/// <summary>
		/// Transforms the source XML data to spreadsheet map XML document.
		/// </summary>
		/// <param name="source">Source XML data.</param>
		/// <param name="template">Name of XSL template to use.</param>
		/// <returns>A LINQ XElement object reference containing the transformed XML data.</returns>
		private XElement TransformXML(string source, string year, string template)
		{
			XElement mappedXml = null;

			if (string.IsNullOrWhiteSpace(source) || string.IsNullOrWhiteSpace(template))
			{
				return mappedXml;
			}

			string xslFilename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, string.Format(_transformationFilenameTemplate, year, template));
			if (File.Exists(xslFilename))
			{
				StringReader reader = new StringReader(source);
				XPathDocument sourceXml = new XPathDocument(reader);
				reader.Close();

				// Create and load the transform with script execution enabled.
				XslCompiledTransform transform = new XslCompiledTransform();	// TODO: We may want to cache this guy for performance. Costly to initialize. (FP20110217)
				XsltSettings settings = new XsltSettings();
				settings.EnableScript = true;
				transform.Load(xslFilename, settings, null);

				MemoryStream tempStream = new MemoryStream();
				transform.Transform(sourceXml, null, tempStream);
				tempStream.Seek(0, SeekOrigin.Begin);	// Be kind, rewind. If we don't rewind, stream pointer will be at the end of the stream and next operation will fail.
				mappedXml = XElement.Load(tempStream);	// Create XElement object reference from transformed XML data.
				tempStream.Close();
			}

			return mappedXml;
		}

		/// <summary>
		/// Iterates through the report data to fill out the Excel report.
		/// </summary>
		/// <param name="sourceXML">Parsed data from XML sent within HTTP request.</param>
		/// <param name="workbook">Spreadsheet to fill out.</param>
		/// <returns>True if no errors found or false otherwise.</returns>
		private bool FillReportData(XElement sourceXML, SpreadsheetGear.IWorkbook workbook)
		{
			bool status = false; // Assume it's the end of the world.
			SpreadsheetGear.IWorksheet worksheet = null;
			SpreadsheetGear.IRange cells = null;
			//SpreadsheetGear.Drawing.Color validationFailColor = SpreadsheetGear.Drawing.Color.FromArgb(255, 0, 0);
			bool protectContents = false;
			bool isWorksheetUnprotected = false;

			string worksheetName = null;
			string id = null;
			string cellValue = null;
			XAttribute defaultValueAttribute = null;
			XAttribute typeAttribute = null;
			XAttribute blankTypeAttribute = new XAttribute(_typeAttributeName, string.Empty);	// Reuse empty attribute type whenever possible.

			if (sourceXML == null || workbook == null)
			{
				return false;
			}

			try
			{
				// Select worksheet elements from source XML.
				IEnumerable<XElement> xmlWorksheets =
					from xmlWorksheet in sourceXML.Elements()
					select xmlWorksheet;

				foreach (XElement xmlWorksheet in xmlWorksheets)
				{
					worksheetName = xmlWorksheet.Attribute(_nameAttributeName).Value;
					worksheet = workbook.Worksheets[worksheetName];	// Cache worksheet for later use.
					protectContents = worksheet.ProtectContents;
					cells = worksheet.Cells;	// Cache for later use.

					// Select child elements of each worksheet element from source XML.
					IEnumerable<XElement> xmlCells =
						from xmlCell in xmlWorksheet.Elements()
						select xmlCell;

					foreach (XElement xmlCell in xmlCells)
					{
						defaultValueAttribute = xmlCell.Attribute(_defaultValueAttributeName);
						typeAttribute = xmlCell.Attribute(_typeAttributeName);
						if (typeAttribute == null)
						{
							// Just set reference to blank entry if attribute not present.
							typeAttribute = blankTypeAttribute;
						}

						switch (typeAttribute.Value)
						{
							#region Boolean element
							case _typeBoolAttributeValue:
								// Radio buttons work by setting a value to linked cell.
								// For this implementation, we convert Y/N to 1/2 respectively.
								if (protectContents && !isWorksheetUnprotected)
								{
									// If this is the first radio button in worksheet, unprotect worksheet.
									// We get better results by unprotecting the sheet to prevent UI behavior from kicking in while setting radio button value.
									worksheet.Unprotect(string.Empty);
									isWorksheetUnprotected = true;
								}
								id = xmlCell.Attribute(_idAttributeName).Value;
								cellValue = xmlCell.Attribute(_valueAttributeName).Value;
								if (string.IsNullOrWhiteSpace(cellValue) && defaultValueAttribute != null)
								{
									// If we don't have a value and a default value is defined, use default value as cell value.
									cellValue = defaultValueAttribute.Value;
								}
								cells[id].Value = (string.CompareOrdinal(_typeBoolYesValue, cellValue) == 0) ? _typeBoolYesConversionValue : _typeBoolNoConversionValue;
								break;
							#endregion

							#region Table element
							case _typeTableAttributeValue:
								// Select row elements from table element in source XML.
								IEnumerable<XElement> xmlRows =
									from xmlRow in xmlCell.Elements()
									select xmlRow;

								string tableRangeName = xmlCell.Attribute(_rangeAttributeName).Value;	// Get name of range for table.
								SpreadsheetGear.IRange tableRange = cells[tableRangeName];

								int rowsNeeded = xmlRows.Count();
								int rowsAvailable = tableRange.RowCount;

								// Determine if we need to expand table to enter additional values.
								if (rowsNeeded > rowsAvailable)
								{
									if (protectContents && !isWorksheetUnprotected)
									{
										// We need to unprotect the worksheet to insert rows.
										worksheet.Unprotect(string.Empty);
										isWorksheetUnprotected = true;
									}

									int rowStart = tableRange.Row + 2;
									int rowEnd = rowStart + rowsNeeded - rowsAvailable;
									cells[rowStart + ":" + rowEnd].Insert();	// Expand table by calculated number of rows.									
								}
								tableRange.FillDown();	// Copy formulas, formatting, and values from top row to rest of range. Awesome!

								int rowNumber = tableRange.Row;
								string columnLetter;

								// Fill in table with values from XML. Iterate each row and for each row, iterate each column.
								foreach (XElement xmlRow in xmlRows)
								{
									rowNumber++;	// We increment before loop because row index in Spreadsheet Gear is zero (0) based as opposed to Excel one (1) based.

									// Select cell elements from row element in source XML.
									IEnumerable<XElement> xmlRowCells =
										from xmlRowCell in xmlRow.Elements()
										select xmlRowCell;

									foreach (XElement xmlRowCell in xmlRowCells)
									{
										columnLetter = xmlRowCell.Attribute(_colAttributeName).Value;
										defaultValueAttribute = xmlRowCell.Attribute(_defaultValueAttributeName);
										cellValue = xmlRowCell.Attribute(_valueAttributeName).Value;
										cells[columnLetter + rowNumber].Value = (string.IsNullOrWhiteSpace(cellValue) && defaultValueAttribute != null) ? defaultValueAttribute.Value : cellValue;
									}
								}

								break;
							#endregion

							#region Normal element
							default:
								id = xmlCell.Attribute(_idAttributeName).Value;
								cellValue = xmlCell.Attribute(_valueAttributeName).Value;
								// If we don't have a value and a default value is defined, use default value as cell value.
								cells[id].Value = (string.IsNullOrWhiteSpace(cellValue) && defaultValueAttribute != null) ? defaultValueAttribute.Value : cellValue;
								break;
							#endregion
						}
					}

					if (protectContents && isWorksheetUnprotected)
					{
						// Make sure to protect worksheet if it had been unprotected.
						worksheet.Protect(string.Empty);
						isWorksheetUnprotected = false;
					}
					// Just being nice to users, reset zoom and scroll for each worksheet. Makes document ready to read.
					worksheet.WindowInfo.Zoom = 100;	// Reset zoom level for each worksheet in spreadsheet.
					worksheet.WindowInfo.ScrollRow = 0;	// Reset scroll for worksheet.
					worksheet.Cells["B1"].Select();	// Select left-top cell. Column A is generally collapsed in EU ETS spreadsheet.
					status = true;
				}
			}
			catch
			{
				status = false;
			}

			return status;
		}


        private void LoadCorsia2018()
        {
            CorsiaWorkbooks.Add( 2018, ConfigurationManager.AppSettings["Corsia2018"] );
            Corsia corsia = new Corsia();

            // open the 2018 Corsia spreadsheet
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, _appDataFolder);
            path = Directory.GetFiles( path, CorsiaWorkbooks[2018] ).FirstOrDefault();
            workbook = SpreadsheetGear.Factory.GetWorkbook( path );

            // get ICAO info located in worksheet CERT_Doc7910
            SpreadsheetGear.IRange cells = workbook.Worksheets[11].Cells;

            int rows;
            for( rows = 1; rows < 15000; rows++ )
            {
                if( cells[rows, 0].Value == null )
                    break;

                corsia.Icao.Add( cells[rows, 0].Value.ToString() );
                corsia.Lat.Add( double.Parse( cells[rows, 2].Value.ToString() ) );
                corsia.Lon.Add( double.Parse( cells[rows, 3].Value.ToString() ) );
                corsia.State.Add( cells[rows, 5].Value.ToString() );
            }

            // get aircraft type info located in worksheet CEM_GCD
            Debug.WriteLine( workbook.Worksheets[13].Name );
            cells = workbook.Worksheets[13].Cells;

            for( rows = 1; rows < 200; rows++ )
            {
                if( cells[rows, 0].Value == null )
                    break;

                corsia.AcType.Add(                    cells[rows, 0].Value.ToString() );
                corsia.Intercept1.Add(  double.Parse( cells[rows, 1].Value.ToString() ) );
                corsia.Slope1.Add(      double.Parse( cells[rows, 2].Value.ToString() ) );
                corsia.Breakpoint1.Add( double.Parse( cells[rows, 3].Value.ToString() ) );
                corsia.Intercept2.Add(  double.Parse( cells[rows, 4].Value.ToString() ) );
                corsia.Slope2.Add(      double.Parse( cells[rows, 5].Value.ToString() ) );
                corsia.Breakpoint2.Add( double.Parse( cells[rows, 6].Value.ToString() ) );
                corsia.Intercept3.Add(  double.Parse( cells[rows, 7].Value.ToString() ) );
                corsia.Slope3.Add(      double.Parse( cells[rows, 8].Value.ToString() ) );
                corsia.Segments.Add(      byte.Parse( cells[rows, 9].Value.ToString() ) );
            }

            workbook.Close();
            CorsiaData.Add( 2018, corsia );

        }

        private void LoadCorsia2019()
        {
            CorsiaWorkbooks.Add( 2019, ConfigurationManager.AppSettings["Corsia2019"] );
            Corsia corsia = new Corsia();

            // open the 2019 Corsia spreadsheet
            string path = Path.Combine( AppDomain.CurrentDomain.BaseDirectory, _appDataFolder );
            path = Directory.GetFiles( path, CorsiaWorkbooks[2019] ).FirstOrDefault();
            workbook = SpreadsheetGear.Factory.GetWorkbook( path );

            // get ICAO info located in worksheet CERT_Doc7910
            SpreadsheetGear.IRange cells = workbook.Worksheets[30].Cells;

            int rows;
            for( rows = 1; rows < 15000; rows++ )
            {
                if( cells[rows, 0].Value == null )
                    break;

                corsia.Icao.Add( cells[rows, 0].Value.ToString() );
                corsia.Lat.Add( double.Parse( cells[rows, 2].Value.ToString() ) );
                corsia.Lon.Add( double.Parse( cells[rows, 3].Value.ToString() ) );
                corsia.State.Add( cells[rows, 5].Value.ToString() );
            }

            // get aircraft type info located in worksheet CEM_GCD
            Debug.WriteLine( workbook.Worksheets[33].Name );
            cells = workbook.Worksheets[33].Cells;

            for( rows = 1; rows < 200; rows++ )
            {
                if( cells[rows, 0].Value == null )
                    break;

                corsia.AcType.Add(                    cells[rows, 0].Value.ToString() );
                corsia.Intercept1.Add(  double.Parse( cells[rows, 1].Value.ToString() ) );
                corsia.Slope1.Add(      double.Parse( cells[rows, 2].Value.ToString() ) );
                corsia.Breakpoint1.Add( double.Parse( cells[rows, 3].Value.ToString() ) );
                corsia.Intercept2.Add(  double.Parse( cells[rows, 4].Value.ToString() ) );
                corsia.Slope2.Add(      double.Parse( cells[rows, 5].Value.ToString() ) );
                corsia.Breakpoint2.Add( double.Parse( cells[rows, 6].Value.ToString() ) );
                corsia.Intercept3.Add(  double.Parse( cells[rows, 7].Value.ToString() ) );
                corsia.Slope3.Add(      double.Parse( cells[rows, 8].Value.ToString() ) );
                corsia.Segments.Add(      byte.Parse( cells[rows, 9].Value.ToString() ) );
            }

            workbook.Close();
            CorsiaData.Add( 2019, corsia );
        }


        #endregion

    }
}
