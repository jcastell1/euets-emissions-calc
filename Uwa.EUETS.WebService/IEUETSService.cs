﻿//------------------------------------------------------------------------------
// Enterprise Applications Group
// Universal Weather and Aviation, Inc.
//
// © 2011 Universal Weather and Aviation, Inc. All Rights Reserverd.
//------------------------------------------------------------------------------
// This file contains proprietary information and cannot be used or distributed
// without explicit consent from Universal Weather and Aviation, Inc.
//------------------------------------------------------------------------------

using System;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace Uwa.EUETS
{
	/// <summary>
	/// Service contract for EUETS web service.
	/// <para>
	/// The EU ETS service provides methods for calculating and reporting emissions as mandated by the EU.
	/// </para>
	/// </summary>
	[ServiceContract]
	public interface IEUETSService : IDisposable
	{
        /// <summary>
        /// Gets the calculated emission for the given parameters.
        /// </summary>
        /// <param name="aircraftType">String representing an Aircraft type.</param>
        /// <param name="distance">String representing Distance travelled. String must contain an integer value.</param>
        /// <param name="year">String representing the year of the emmisions</param>
        /// <returns>
        /// Returns a stream in the format "value|value". The pipe character '|' is the delimiter.
        /// <para>
        /// If successful, returns values represent estimated fuel consumption and CO2 emissions.
        /// Both values are in Kilograms (Kg).
        /// </para>
        /// <para>
        /// On failure, the returned string will have the format '-1|CAUSE OF ERROR'.
        /// The error is marked by the first value containing a -1. The text after the pipe character
        /// contains more information about the error that occurred.
        /// </para>
        /// </returns>
        [OperationContract]
        [WebGet(BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "GetEmission?at={aircraftType}&d={distance}&y={year}")]
        Stream GetEmission(string aircraftType, string distance, string year);


        /// <summary>
        /// Gets the calculated emission from the CORSIA spreadsheet for the given parameters
        /// </summary>
        /// <param name="aircraftType">String representing an Aircraft type.</param>
        /// <param name="origin">The origin ICAO</param>
        /// <param name="destination">The destination ICAO</param>
        /// <param name="year">Year of the emissions</param>
        /// <returns>
        /// Returns a stream in the format "value|value|value". 
        /// The pipe character '|' is the delimiter.
        /// <para>
        /// If successful, returns values representing the great circle distance (km), CO2 emissions (metric tonnes),
        /// and whether the flight is subject to the scope of CORSIA
        /// </para>
        /// <para>
        /// On failure, the returned string will have the format '-1|CAUSE OF ERROR'.
        /// The error is marked by the first value containing a -1. The text after the pipe character
        /// contains more information about the error that occurred.
        /// </para>
        /// </returns>
        [OperationContract]
        [WebGet( BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "GetCorsiaEmission?actype={aircraftType}&orig={origin}&dest={destination}&year={year}" )]
        Stream GetCorsiaEmission( string aircraftType, string origin, string destination, string year );



        /// <summary>
        /// Returns an EU ETS emissions report as an Excel file.
        /// <para>
        /// Incoming report data is sent via the HTTP request message as XML embedded as the HTTP message body.
        /// </para>
        /// </summary>
        /// <param name="t">Name of report template to use. Current values can be "aem" and "tkm".</param>
        /// <remarks>
        /// Template names are used as part of the file names used to determine which Excel file and which XSLT file
        /// to use. If these files are not located in the App_Data folder, the call will fail.
        /// </remarks>
        [OperationContract]
		[WebInvoke(Method = "POST", UriTemplate = "GetReport?t={template}&y={year}")]
		Stream GetReport(string template, string year);
	}
}
