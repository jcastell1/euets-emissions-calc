﻿//------------------------------------------------------------------------------
// Enterprise Applications Group
// Universal Weather and Aviation, Inc.
//
// © 2011 Universal Weather and Aviation, Inc. All Rights Reserved.
//------------------------------------------------------------------------------
// This file contains proprietary information and cannot be used or distributed
// without explicit consent from Universal Weather and Aviation, Inc.
//------------------------------------------------------------------------------

using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Uwa.EUETS.WebService")]
[assembly: AssemblyDescription("UWA EU ETS Web Service")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Universal Weather and Aviation, Inc.")]
[assembly: AssemblyProduct("Enterprise Applications")]
[assembly: AssemblyCopyright( "© 2019 Universal Weather and Aviation, Inc." )]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("4490e918-f992-445f-803a-120f1110fe06")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion( "3.0.2.0" )]
